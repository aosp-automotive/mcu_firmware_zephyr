/*
 *  HID generic sample for 96carboard MCU.
 *
 *  What this sample does, is it opens the device, claims the third
 *  interface (idx = 2), which is the "configuration" interface,
 *  sends a command 0x0101 to start a SWI dump, reads 10 replies,
 *  and sends a command 0x0102 to stop the SWI dump.
 *
 *  compile with "gcc -o hid-sample hid-sample.c -lusb-1.0"
 */

#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <libusb-1.0/libusb.h>

// With firmware support, transfers can be > the endpoint's max packet size.
static const int MAX_INTERRUPT_IN_TRANSFER_SIZE = 0x10;
static const int MAX_INTERRUPT_OUT_TRANSFER_SIZE = 0x2;

static const int VENDOR_ID = 0x16c0;
static const int PRODUCT_ID = 0x05dc;
static const int INTERFACE_NUMBER = 2; // 0-based interface index.

static const int INTERRUPT_IN_ENDPOINT = 0x81+INTERFACE_NUMBER;
static const int INTERRUPT_OUT_ENDPOINT = 0x01+INTERFACE_NUMBER;

static const int TIMEOUT_MS = 5000;

// Use interrupt transfers to to write data to the device and receive data from the device.
// Returns - zero on success, libusb error code on failure.
int exchange_input_and_output_reports_via_interrupt_transfers(libusb_device_handle *devh){

	int bytes_transferred;
	int i = 0, j;
	int result = 0;;

 	uint8_t data_in[MAX_INTERRUPT_IN_TRANSFER_SIZE];
	uint8_t data_out[MAX_INTERRUPT_OUT_TRANSFER_SIZE];

	// Store data in a buffer for sending.
	data_out[0] = 0x01;
	data_out[1] = 0x01;

	// Write data to the device.
	result = libusb_interrupt_transfer(
			devh,
			INTERRUPT_OUT_ENDPOINT,
			data_out,
			MAX_INTERRUPT_OUT_TRANSFER_SIZE,
			&bytes_transferred,
			TIMEOUT_MS);

	if (result >= 0){
	  	printf("Data sent via interrupt transfer:\n");
	  	for(i = 0; i < bytes_transferred; i++){
	  		printf("%02x ",data_out[i]);
	  	}
	  	printf("\n");

		// Read data from the device.
		for (j = 0; j < 10; j++){
			result = libusb_interrupt_transfer(
					devh,
					INTERRUPT_IN_ENDPOINT,
					data_in,
					MAX_INTERRUPT_IN_TRANSFER_SIZE,
					&bytes_transferred,
					TIMEOUT_MS);

			if (result >= 0){
				if (bytes_transferred > 0){
				  	printf("Data received via interrupt transfer:\n");
				  	for(i = 0; i < bytes_transferred; i++){
				  		printf("%02X ",data_in[i]);
				  	}
				  	printf("\n");
				} else {
					fprintf(stderr, "No data received in interrupt transfer (%d)\n", result);
					return result;
				}
			} else {
				fprintf(stderr, "Error receiving data via interrupt transfer %d\n", result);
				return result;
			}
		}
	} else {
		fprintf(stderr, "Error sending data via interrupt transfer %d\n", result);
		return result;
	}

	data_out[1] = 0x02;
	result = libusb_interrupt_transfer(
			devh,
			INTERRUPT_OUT_ENDPOINT,
			data_out,
			MAX_INTERRUPT_OUT_TRANSFER_SIZE,
			&bytes_transferred,
			TIMEOUT_MS);

	if (result >= 0){
	  	printf("Data sent via interrupt transfer:\n");
	  	for(i = 0; i < bytes_transferred; i++){
	  		printf("%02x ",data_out[i]);
	  	}
	  	printf("\n");
	} else return result;

  	return 0;
}

int main(void){
	struct libusb_device_handle *devh = NULL;
	int device_ready = 0;
	int result;

	result = libusb_init(NULL);
	if (result >= 0){
		devh = libusb_open_device_with_vid_pid(NULL, VENDOR_ID, PRODUCT_ID);

		if (devh != NULL){
			// The HID has been detected.
			// Detach the hidusb driver from the HID to enable using libusb.
			libusb_detach_kernel_driver(devh, INTERFACE_NUMBER);
			result = libusb_claim_interface(devh, INTERFACE_NUMBER);
			if (result >= 0){
				device_ready = 1;
			} else {
				fprintf(stderr, "libusb_claim_interface error %d\n", result);
			}
		} else {
			fprintf(stderr, "Unable to find the device.\n");
		}
	} else {
		fprintf(stderr, "Unable to initialize libusb.\n");
	}

	if (device_ready) {
		// Send and receive data.
		result = exchange_input_and_output_reports_via_interrupt_transfers(devh);
		if (result) fprintf(stderr, "error in transfer: %d\n", result);

		// Finished using the device.
		libusb_release_interface(devh, 0);
	}
	libusb_close(devh);
	libusb_exit(NULL);
	return 0;
}

