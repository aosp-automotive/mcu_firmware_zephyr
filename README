96carboard firmware source code for Zephyr RTOS
===============================================

This project is compatible with release branch "v2.3-branch" of Zephyr RTOS.

This project can also be built against zephyr master branch, requiring, at
June 8, 2020, NO changes either to zephyr, or to this project. However,
given the rate that zephyr master advances, there is no guarantee that it
will continue to be so for long. It is not my intention to perpetually
maintain support for zephyr master.

To build:
Enter the directory containing this file and type;
west build

To install:
Enter the directory from where the project was built and type;
west flash
or
bossac --port=/dev/ttyACM0 --offset=0x2000 -e -w -v build/zephyr/zephyr.bin -R

OR to preserve NVS;
bossac --port=/dev/ttyACM0 --offset=0x2000 -w -v build/zephyr/zephyr.bin -R


96carboard pin definitions
==========================

PA02: I/O CONN202-1 (IN ADC SWI1)
PA03: I/O CONN202-2 (IN ADC SWI2)
PA04: I/O CONN202-3 (IN ADC SWI1)
PA05: I/O CONN202-4 (IN ADC SWI2)
PA06: I/O CONN202-5
PA07: I/O CONN202-6 (Backlight PWM)
PA08: I/O CONN202-7 (OUT PWM Fan)
PA09: I/O CONN202-8 (IN Tach Fan)
PA10: OUT 14V switch
PA11: OUT 5V switch
PA14: OUT AMP Un-mute
PA15: OUT AMP Enable
PA16: I/O CONN203-2 (I2C_0)
PA17: I/O CONN203-3 (I2C_0)
PA18: I/O CONN203-4 (INT for I2C_0)
PA19: OUT SBC Reset
PA22: OUT 3.3V Reg Enable
PA23: IN Ignition
PA27: IN Headlight
PA28: IN Reverse


Regarding SWI inputs...
=======================

The SAMD21 has only one ADC, but which can be programmed to any of
multiple input channels. ADC inputs can be "scanned" in sequence to
a certain extent, as in ain0->ain1->ain2 etc. and stored in
ADC->RESULT.bit.RESULT[n] from 0-15, however, the ADC driver for
zephyr lacks this feature.

The chip also has a pair of Analog Comparators, which can
generate interrupts based on programmable voltage thresholds.

So the right way to do this, is to attach one AC to each of SWI1 and
SWI2. When an interrupt occurs, sample the ADC applicable to the AC
that generated the interrupt.

Now unfortunately, the AC can only be connected to PA04-PA07, and
the SWI's are hooked up to PA02 and PA03. On older revision boards,
this means placing a physical jumper from PA02->PA04 and PA03->PA05.
and, deactivating PA02 and PA03, and using PA04 and PA05 instead.
This will be corrected in the next board revision in order to free
up PA02 and PA03 for other use. After this adjustment, there remains
PA06, PA07, PA17, and PA18 for alternate functions such as GT911.

Console is attached to PA30 and PA31.

I2C is only available on PA08/PA09, PA16/PA17, PA22/PA23. This
means that on 96carboard, it has to be PA16/PA17, requiring the
backlight PWM to be moved from PA16 to PA07. This will leave the
entire connector CONN203 (PA16, PA17, PA18) available for I2C+Int,
which will suffice for GT911.


USB....
=======

The USB on this device is full speed 12 Mbps USB 2.0.
Max 16 endpoints (8 endpoint addresses, in pairs input and output).

Allocate as:
-- Logging and config: 1 pair
-- Fan control: 1 pair
-- Backlight control: 1 pair
-- SWI: 1 pair
-- Touch: 1 pair

Interrupt: NVIC Line 7.


On ACM vs HIDRAW:
=================

Each ACM requires 3 half-endpoints. Interrupt-IN, Data-IN, Data-OUT.
This is why all the EP's are used up when using 3xACM + 2xHID.

Control		IN-1
Control		OUT-1
ACM1 INT	IN-2
ACM1 DATA	IN-3
ACM1 DATA	OUT-2
ACM2 INT	IN-4
ACM2 DATA	IN-5
ACM2 DATA	OUT-3
ACM3 INT	IN-6
ACM3 DATA	IN-7
ACM3 DATA	OUT-4
HID1		IN-8
HID2		IN-9 <--	not possible.

Since this application requires a total of 5 devices (control keys, touch,
config, backlight, fan), ACM is not an option. Each HID device requires only
a single pair of endpoints. 5 HID endpoint pairs is less than the 7 available
(8 total including USB-control EP's) thus this configuration is possible.

The added simplicity and robustness of HID also makes this approach ideal.


NVS / Settings storage:
=======================

*** IMPORTANT: If bossac is run withOUT the "-e" parameter, the flash
region containing the NVS data will be preserved through a rewrite.


NVS stores variable length blobs of binary data according to an ID KEY.

Need to store settings for the display backlight, touchscreen, and for the
steering wheel buttons.

None of the data needs to be changed frequently -- set once and forget.

Touchscreen:
uint8_t enabled
uint8_t add0x14
uint8_t reset
uint8_t inv_x
uint8_t inv_y
uint8_t swap_xy
uint16_t b_left
uint16_t b_right
uint16_t b_top
uint16_t b_bottom
ID=1

The backlight may require a minimum brightness,
uint8_t bl_min
ID=2

The number of SWI keys is unknown, but they are of constant length;
uint8_t adc
uint16_t val
uint16_t key

ID=10 will be the number of keys.
ID=11..n will be the individual keys.

Note: there is a gap in the IDs from 3-9 inclusive.


GPIO's:
=======

PA10: OUT 14V switch:		ON initially, ON with IGN, OFF 30 sec after IGN-OFF
PA11: OUT 5V switch:		ON initially, ON with IGN, OFF 30 sec after IGN-OFF
PA14: OUT AMP Un-mute:		Turn on when USB connects.
PA15: OUT AMP Enable:		Turn on when USB connects.
PA22: OUT 3.3V Reg Enable:	Always ON/HIGH.
PA23: IN Ignition:		INPUT, No-pull
PA27: IN Headlight:		INPUT, No-pull
PA28: IN Reverse:		INPUT, No-pull


CONFIGURATION:
==============

The device is configured over HID using;
VENDOR ID: 0x16c0
PRODUCT ID: 0x05dc
INTERFACE IDX: 0x2
USAGE PAGE: 0xff01
USAGE: 0x0300

Require several commands for programming;
Note: First byte is the report ID.
0x01 01: begin sending ADC values.
	Which will be output as report ID followed by 4-bytes.
	2 bytes each for ADC0 and 1.
0x01 02: stop sending ADC values.
0x01 03: set touchscreen config, followed by 14 byte configuration
	described in NVS settings above.
0x01 04: set backlight min, followed by 1 byte.
0x01 05: wipe keys
0x01 06: dump keys
	Which will be output as separate report per each 5-byte entry.
0x01 07: add key, followed by 5-byte entry
0x01 08: replace key, followed by 0-based IDX, and then 5-byte entry.
0x01 09: reboot (for reloading configuration)
0x01 0a: reboot-bootloader (for firmware upgrades)


Backlight control:
==================

If the display in use has a PWM backlight control that operates at 3.3v,
it can be attached to PA07 at pin 6 of CONN202.

Backlight can be controlled over HID using;
VENDOR ID: 0x16c0
PRODUCT ID: 0x05dc
INTERFACE IDX: 0x3
USAGE PAGE: 0xff01
USAGE: 0x0301

Data format:
0x01 XX: where XX is the duty cycle. The period is set to 0xff.


Fan control:
============

A PWM fan plugged into the fan header can be controlled over HID using;
VENDOR ID: 0x16c0
PRODUCT ID: 0x05dc
INTERFACE IDX: 0x4
USAGE PAGE: 0xff01
USAGE: 0x0302

Data format:
0x01 XX: where XX is the duty cycle. The period is set to 0xff.

