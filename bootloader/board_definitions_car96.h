#ifndef _BOARD_DEFINITIONS_H_
#define _BOARD_DEFINITIONS_H_

/*
 * USB device definitions
 */
#define STRING_PRODUCT "96carboard Bootloader"
#define STRING_MANUFACTURER "https://aprax.tk/auto"
#define USB_VID_HIGH   0x16
#define USB_VID_LOW    0xC0
#define USB_PID_HIGH   0x05
#define USB_PID_LOW    0xe1

/*
 * If BOOT_LOAD_PIN is defined the bootloader is started if the selected
 * pin is tied LOW.
 */
#define BOOT_LOAD_PIN                     PIN_PA04

/* Master clock frequency */
#define CPU_FREQUENCY                     (48000000ul)
#define VARIANT_MCK                       CPU_FREQUENCY

/* Frequency of the board main oscillator */
#define VARIANT_MAINOSC                   (32768ul)
#define CRYSTALLESS                       (1)

/* Calibration values for DFLL48 pll */
#define NVM_SW_CALIB_DFLL48M_COARSE_VAL   (58)
#define NVM_SW_CALIB_DFLL48M_FINE_VAL     (64)

/*
 * LEDs definitions
 */
// PA07
#define BOARD_LED_PORT                    (0)
#define BOARD_LED_PIN                     (7)

#define BOARD_LEDRX_PORT		  (0)
#define BOARD_LEDRX_PIN			  (10)
#define BOARD_LEDTX_PORT		  (0)
#define BOARD_LEDTX_PIN			  (11)

#endif // _BOARD_DEFINITIONS_H_
