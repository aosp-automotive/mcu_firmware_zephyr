#define MULTI_TOUCH_DATA_REPORT_ID		0x01
#define CONSUMER_KEY_REPORT_ID			0x04

#define HID_RAW_REPORT_DESC(idx) {				\
	0x06, 0x01, 0xff,					\
	0x0a, idx, 0x03,					\
	HID_MI_COLLECTION, COLLECTION_APPLICATION,		\
	HID_GI_LOGICAL_MIN(1), 0x00,				\
	HID_GI_LOGICAL_MAX(2), 0xFF, 0x00,			\
	HID_GI_REPORT_ID, 0x01,					\
	HID_GI_REPORT_SIZE, 0x08,				\
	HID_GI_REPORT_COUNT, 0x01,				\
	HID_LI_USAGE, USAGE_GEN_DESKTOP_UNDEFINED,		\
	HID_MI_INPUT, 0x82,					\
	HID_GI_REPORT_ID, 0x02,					\
	HID_GI_REPORT_SIZE, 0x08,				\
	HID_GI_REPORT_COUNT, 0x01,				\
	HID_LI_USAGE, USAGE_GEN_DESKTOP_UNDEFINED,		\
	HID_MI_OUTPUT, 0x82,					\
	HID_MI_COLLECTION_END,					\
}

static const uint8_t hid_config_report_desc[] = HID_RAW_REPORT_DESC(0);
static const uint8_t hid_backl_report_desc[] = HID_RAW_REPORT_DESC(1);
static const uint8_t hid_fan_report_desc[] = HID_RAW_REPORT_DESC(2);

static const uint8_t hid_consumer_report_desc[] = {
	// Transmit as:
	// 0x04 aa bb cc dd
	// where 0x04 is the report id
	// 0xaa is SWI1 LSB
	// 0xbb is SWI1 MSB
	// 0xcc is SWI2 LSB
	// 0xdd is SWI2 MSB
	// Set SWIx 0x0000 for key-up.
	//
	// i.e.
	//
	// key UP report:
	// uint8_t report[] = {0x04, 0x00, 0x00, 0x00, 0x00};
	//
	// SWI1 key vol+ (0x00e9) report:
	// uint8_t report[] = {0x04, 0xe9, 0x00, 0x00, 0x00};
	//
	// SWI2 key vol- (0x00ea) report:
	// uint8_t report[] = {0x04, 0x00, 0x00, 0xea, 0x00};
	//
	// Send report:
	// hid_int_ep_write(hid_dev, report, sizeof(report), NULL);
	//
	//
	// This is set for 2 keys simultaneously, to match the two SWI's.
	//
	// Consumer Control (Sound/Media keys)
	0x05, 0x0C,				// usage page (consumer device)
	0x09, 0x01, 				// usage -- consumer control
	0xA1, 0x01, 				// collection (application)
	0x85, 0x04,				// report id
	0x15, 0x00, 				// logical minimum
	0x26, 0xFF, 0x03, 			// logical maximum (3ff)
	0x19, 0x00, 				// usage minimum (0)
	0x2A, 0xFF, 0x03, 			// usage maximum (3ff)
	0x95, 0x02, 				// report count (2) -- 2 keys
	0x75, 0x10, 				// report size (16)
	0x81, 0x00, 				// input
	0xC0 // end collection
};

