/*
 * Copyright (c) 2020 Adam Serbinski
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include "samd_pwm_driver.h"
#include "gt911_driver.h"
#include <device.h>
#include <drivers/adc.h>
#include <drivers/flash.h>
#include <drivers/uart.h>
#include <drivers/watchdog.h>
#include <fs/nvs.h>
#include <logging/log.h>
#include <power/reboot.h>
#include <soc.h>
#include <stdio.h>
#include <stdlib.h>
#include <storage/flash_map.h>
#include <string.h>
#include <usb/usb_device.h>
#include <usb/class/usb_hid.h>
#include <zephyr.h>
#include "defs.h"

static K_SEM_DEFINE(key_sem, 0, 1);
static K_SEM_DEFINE(ign_sem, 0, 1);
static K_SEM_DEFINE(swi_sem, 0, 1);

static struct nvs_fs fs;

#define TOUCH_CONF_ID		1
#define BACKLIGHT_CONF_ID	2
#define SWI_CNT_ID		10
#define SWI_DATA_START_ID	11
#define SWI_TOLERANCE		16

#define PIN_IN_PA02		2
#define PIN_IN_PA03		3
#define PIN_IN_PA04		4
#define PIN_IN_PA05		5

#define PIN_OUT_14V		10
#define PIN_OUT_5V		11
#define PIN_OUT_UNMUTE		14
#define PIN_OUT_AMP		15
#define PIN_OUT_3V3		22
#define PIN_IN_IGN		23
#define PIN_IN_HEADLIGHT	27
#define PIN_IN_REVERSE		28

#define PIN_PWM_BACKLIGHT	7
#define PIN_PWM_FAN		8

// VSCALER_VAL is linear starting at 0.05v with steps of 0.05v.
// Hyst is set at approximately 0.03v
// 2.5v / 0.05 = 50 or 0x32. (actually measures to 2.58v)
#define VSCALER_VAL 0x32;

#define PERIOD 0xff
static uint8_t backlight_min = 0;

static struct device *pwm0;
static struct device *pwm1;
static struct device *adc;
static struct device *i2c;
static struct device *porta;

static int wdt_channel_id;
static struct device *wdt;

static struct device *hid_key;
static struct device *hid_touch;
static struct device *hid_conf;
static struct device *hid_backl;
static struct device *hid_fan;

static struct device *gt911;

static int usb_ready = 0;
static int dev_ready = 0;

#define BUFFER_SIZE  1
static s16_t m_sample_buffer[BUFFER_SIZE];
const struct adc_sequence sequence = {
	.channels    = BIT(0),
	.buffer      = m_sample_buffer,
	.buffer_size = sizeof(m_sample_buffer),
	.resolution  = 12,
};

static struct adc_channel_cfg m_channel_cfg = {
        .gain             = ADC_GAIN_1_2,	// Scale input to half.
        .reference        = ADC_REF_VDD_1,	// Set ref to VDDANA/2
        .acquisition_time = ADC_ACQ_TIME_DEFAULT,
        .channel_id       = 0,
        .input_positive   = ADC_INPUTCTRL_MUXPOS_PIN4,
};

struct touchscreen_config {
	uint8_t enabled;
	uint8_t add0x14;
	uint8_t reset;
	uint8_t inv_x;
	uint8_t inv_y;
	uint8_t swap_xy;
	uint16_t b_left;
	uint16_t b_right;
	uint16_t b_top;
	uint16_t b_bottom;
};
static struct touchscreen_config tsconf = {
	.enabled = 0,
	.add0x14 = 0,
	.reset = 0,
	.inv_x = 0,
	.inv_y = 0,
	.swap_xy = 0,
	.b_left = 0,
	.b_right = 0,
	.b_top = 0,
	.b_bottom = 0,
};

typedef struct adc_config {
	uint8_t swi;
	uint16_t val;
	uint16_t key;
} swi_key;

static uint8_t swi_num = 0;
static swi_key swi_keys[32];
static uint8_t swidump = 0;

void reboot_bootloader(){
	// This performs a partial erase of NVM. The part erased
	// is checked by the bootloader to decide how to boot.
	while (!(NVMCTRL->INTFLAG.reg & NVMCTRL_INTFLAG_READY));
	NVMCTRL->STATUS.reg |= NVMCTRL_STATUS_MASK;
	NVMCTRL->ADDR.reg  = 0x1000;
	NVMCTRL->CTRLA.reg = NVMCTRL_CTRLA_CMD_ER | NVMCTRL_CTRLA_CMDEX_KEY;
	while (!(NVMCTRL->INTFLAG.reg & NVMCTRL_INTFLAG_READY));
	NVIC_SystemReset();
}

void ac_isr(void *arg){
	if (AC->INTFLAG.bit.COMP0 == 1) AC->INTFLAG.reg |= AC_INTFLAG_COMP0;
	if (AC->INTFLAG.bit.COMP1 == 1) AC->INTFLAG.reg |= AC_INTFLAG_COMP1;

	printk("Running ISR, mask = %02X...\n", (uint8_t)AC->INTFLAG.reg);
	k_sem_give(&key_sem);
}

void setup_ac(){
	//Disable everything related to AC
	AC->INTENCLR.bit.COMP0		= 1;
	AC->INTENCLR.bit.COMP1		= 1;
	AC->CTRLA.bit.ENABLE		= 0;
	AC->COMPCTRL[0].bit.ENABLE	= 0;
	AC->COMPCTRL[1].bit.ENABLE	= 0;
	AC->INTFLAG.reg |= AC_INTFLAG_COMP0 & AC_INTFLAG_COMP1;

	GCLK->CLKCTRL.reg = GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN_GCLK0 | GCLK_CLKCTRL_ID_AC_DIG_Val;
	while (GCLK->STATUS.bit.SYNCBUSY);
	PM->APBCMASK.bit.AC_		= 1;

	//Setting for AC
	AC->COMPCTRL[0].bit.SINGLE	= 0;
	AC->COMPCTRL[0].bit.HYST	= 1;
	AC->COMPCTRL[0].bit.FLEN	= 0x5; //filter length for hyst
	AC->SCALER[0].bit.VALUE		= VSCALER_VAL;
	AC->COMPCTRL[0].bit.MUXNEG	= 0x5; //vscaler
	AC->COMPCTRL[0].bit.MUXPOS	= 0x0; //ain[0]=PA04
	AC->COMPCTRL[0].bit.OUT		= 0x1;
	AC->COMPCTRL[1].bit.SINGLE	= 0;
	AC->COMPCTRL[1].bit.HYST	= 1;
	AC->COMPCTRL[1].bit.FLEN	= 0x5;
	AC->SCALER[1].bit.VALUE		= VSCALER_VAL;
	AC->COMPCTRL[1].bit.MUXNEG	= 0x5;
	AC->COMPCTRL[1].bit.MUXPOS	= 0x1; //ain[1]=PA05
	AC->COMPCTRL[1].bit.OUT		= 0x1;

	AC->CTRLA.bit.RUNSTDBY		= 1;
	AC->CTRLA.bit.LPMUX		= 1;

	//Set AC & Comparator interrupts
	AC->COMPCTRL[0].bit.INTSEL	= 0;
	AC->INTENSET.bit.COMP0		= 1;
	AC->COMPCTRL[1].bit.INTSEL	= 0;
	AC->INTENSET.bit.COMP1		= 1;
	NVIC_EnableIRQ(AC_IRQn);

	// Enable comparator
	AC->COMPCTRL[0].bit.ENABLE	= 1;
	while (AC->STATUSB.bit.SYNCBUSY){}
	AC->COMPCTRL[1].bit.ENABLE	= 1;
	while (AC->STATUSB.bit.SYNCBUSY){}

	//Enable the AC and individual comparator
	AC->CTRLA.bit.ENABLE		= 1;

	while (AC->STATUSB.bit.SYNCBUSY){}
	AC->INTFLAG.reg |= AC_INTFLAG_COMP0 & AC_INTFLAG_COMP1;
	while (AC->STATUSB.bit.SYNCBUSY){}
}

static void in_ready_cb(void){
	printk("in_ready_cb\n");
}

void swi_thread(void){
	uint8_t swirprt[5];
	uint8_t i;
	printk("swi_thread: started\n");
	swirprt[0] = 0x01;
	while (true){
		printk("Waiting for swi dump request.\n");
		k_sem_take(&swi_sem, K_FOREVER);
		swidump = 1;
		while (swidump){
			m_channel_cfg.input_positive = ADC_INPUTCTRL_MUXPOS_PIN4;
			adc_channel_setup(adc, &m_channel_cfg);
			for (i=0; i<10; i++)
			if (adc_read(adc, &sequence) == 0)
				printk("ADC PA04(%d): 0x%04x\n", i, m_sample_buffer[0]);

			swirprt[1] = m_sample_buffer[0] >> 8;
			swirprt[2] = m_sample_buffer[0] & 0xff;

			m_channel_cfg.input_positive = ADC_INPUTCTRL_MUXPOS_PIN5;
			adc_channel_setup(adc, &m_channel_cfg);
			for (i=0; i<10; i++)
			if (adc_read(adc, &sequence) == 0)
				printk("ADC PA05(%d): 0x%04x\n", i, m_sample_buffer[0]);

			swirprt[3] = m_sample_buffer[0] >> 8;
			swirprt[4] = m_sample_buffer[0] & 0xff;

			hid_int_ep_write(hid_conf, swirprt, sizeof(swirprt), NULL);

			k_sleep(K_SECONDS(1));
		}
		k_sem_reset(&swi_sem);
	}
}

static void out_ready_cb_conf(void){
	// USAGE PAGE: 0xFF01
	// USAGE: 0x0300
	printk("out_ready_cb - conf\n");
	uint8_t data[128];
	uint8_t swirprt[6];
	int ret, len, i;
	ret = hid_int_ep_read(hid_conf, data, 128, &len);
	printk("read %d bytes, status: %d, data: 0x", len, ret);
	for (i=0; i<len; i++){
		printk("%02X ", data[i]);
	}
	printk("\n");
	if (len < 2 || data[0] != 0x01) return;

	if (len == 2){
		switch (data[1]){
		case 0x01:
			// begin SWI dump
			k_sem_give(&swi_sem);
			break;
		case 0x02:
			// end SWI dump
			swidump = 0;
			break;
		case 0x05:
			swi_num = 0;
			nvs_write(&fs, SWI_CNT_ID, &swi_num, sizeof(swi_num));
			break;
		case 0x06:
			for (i = 0; i < swi_num; i++){
				swirprt[0] = 0x02; // report ID
				swirprt[1] = swi_keys[i].swi;
				swirprt[2] = swi_keys[i].val >> 8;
				swirprt[3] = swi_keys[i].val & 0xff;
				swirprt[4] = swi_keys[i].key >> 8;
				swirprt[5] = swi_keys[i].key & 0xff;
				hid_int_ep_write(hid_conf, swirprt, sizeof(swirprt), NULL);
			}
			break;
		case 0x09:
			sys_reboot(0);
			break;
		case 0x0a:
			reboot_bootloader();
		}
	} else if (data[1] == 0x03 && len == 16){
		// set touchscreen config
		tsconf.enabled = data[2];
		tsconf.add0x14 = data[3];
		tsconf.reset = data[4];
		tsconf.inv_x = data[5];
		tsconf.inv_y = data[6];
		tsconf.swap_xy = data[7];
		tsconf.b_left = (data[8] << 8) + data[9];
		tsconf.b_right = (data[10] << 8) + data[11];
		tsconf.b_top = (data[12] << 8) + data[13];
		tsconf.b_bottom = (data[14] << 8) + data[15];
		nvs_write(&fs, TOUCH_CONF_ID, &tsconf, sizeof(tsconf));
	} else if (data[1] == 0x04 && len == 3){
		// set backlight minimum
		backlight_min = data[2];
		nvs_write(&fs, BACKLIGHT_CONF_ID, &backlight_min, sizeof(backlight_min));
	} else if ((data[1] == 0x07 && len == 7) || (data[1] == 0x08 && len == 8)){
		// add or replace a key
		uint8_t replace = (len == 8);
		swi_key m_key;
		m_key.swi = data[replace+2];
		m_key.val = (data[replace+3] << 8) + data[replace+4];
		m_key.key = (data[replace+5] << 8) + data[replace+6];
		nvs_write(&fs, SWI_DATA_START_ID + (replace ? data[2] : swi_num), &m_key, sizeof(m_key));
		if (!replace){
			swi_num++;
			nvs_write(&fs, SWI_CNT_ID, &swi_num, sizeof(swi_num));
		}
	}
}

static void out_ready_cb_backl(void){
	// USAGE PAGE: 0xFF01
	// USAGE: 0x0301
	// Report ID is 0x01, Report length is 0x01
	// Total bytes read per report should be 0x02.
	printk("out_ready_cb - backlight\n");
	uint8_t data[128];
	int ret, len, i;
	ret = hid_int_ep_read(hid_backl, data, 128, &len);
	printk("read %d bytes, status: %d, data: 0x", len, ret);
	for (i=0; i<len; i++){
		printk("%02X ", data[i]);
	}
	printk("\n");
	if (len == 2 && data[0] == 0x01){
		if (backlight_min > 0)
			data[1] = backlight_min + data[1] * (0xff - backlight_min) / 0xff;
		printk("Report is valid, setting backlight to %02X\n", data[1]);
		samd_pwm_setpwm(pwm1, 0, PIN_PWM_BACKLIGHT, PERIOD, 0xff-data[1]);
	}
}

static void out_ready_cb_fan(void){
	// USAGE PAGE: 0xFF01
	// USAGE: 0x0302
	// Report ID is 0x01, Report length is 0x01
	// Total bytes read per report should be 0x02.
	printk("out_ready_cb - fan\n");
	uint8_t data[128];
	int ret, len, i;
	ret = hid_int_ep_read(hid_fan, data, 128, &len);
	printk("read %d bytes, status: %d, data: 0x", len, ret);
	for (i=0; i<len; i++){
		printk("%02X ", data[i]);
	}
	printk("\n");
	if (len == 2 && data[0] == 0x01){
		printk("Report is valid, setting backlight to %02X\n", data[1]);
		samd_pwm_setpwm(pwm0, 0, PIN_PWM_FAN, PERIOD, data[1]);
	}
}

static const struct hid_ops ops = {
	.int_in_ready = in_ready_cb,
};

static const struct hid_ops ops_conf = {
	.int_in_ready = in_ready_cb,
	.int_out_ready = out_ready_cb_conf,
};

static const struct hid_ops ops_backl = {
	.int_in_ready = in_ready_cb,
	.int_out_ready = out_ready_cb_backl,
};

static const struct hid_ops ops_fan = {
	.int_in_ready = in_ready_cb,
	.int_out_ready = out_ready_cb_fan,
};

static void status_cb(enum usb_dc_status_code status, const uint8_t *param){
	switch (status) {
	case USB_DC_ERROR:
		printk("USB device error\n");
		break;
	case USB_DC_RESET:
		printk("USB device reset detected\n");
		break;
	case USB_DC_CONNECTED:
		printk("USB device connected\n");
		break;
	case USB_DC_CONFIGURED:
		printk("USB device configured\n");
		usb_ready = 1;
		if (tsconf.enabled) gt911_hid_enable(gt911);
		break;
	case USB_DC_DISCONNECTED:
		printk("USB device disconnected\n");
		break;
	case USB_DC_SUSPEND:
		printk("USB device suspended\n");
		break;
	case USB_DC_RESUME:
		printk("USB device resumed\n");
		break;
	case USB_DC_UNKNOWN:
	default:
		printk("USB unknown state\n");
		break;
	}
}

void gt911_isr(void){
	printk("Running GT911 ISR\n");
}

void init_load_nvs(){
	int i, rc = 0;
	struct flash_pages_info info;

	fs.offset = FLASH_AREA_OFFSET(storage);
	rc = flash_get_page_info_by_offs(device_get_binding("FLASH_CTRL"),
					 fs.offset, &info);
	if (rc)	printk("Unable to get page info");
	fs.sector_size = info.size;
	fs.sector_count = 3U;

	rc = nvs_init(&fs, "FLASH_CTRL");
	if (rc)	printk("Flash Init failed\n");

	rc = nvs_read(&fs, TOUCH_CONF_ID, &tsconf, sizeof(tsconf));
	if (rc > 0) printk("Found touchscreen configuration in NVS.\n");
	else printk("Touchscreen configuration not found, using defaults.\n");

	rc = nvs_read(&fs, BACKLIGHT_CONF_ID, &backlight_min, sizeof(backlight_min));
	if (rc > 0) printk("Found backlight configuration in NVS.\n");
	else printk("Backlight configuration not found, using defaults.\n");

	rc = nvs_read(&fs, SWI_CNT_ID, &swi_num, sizeof(swi_num));
	printk("Found %d SWI keys defined in NVS.\n", swi_num);

	if (swi_num > 0){
		if (swi_num > 32) swi_num = 32;
		for (i = 0; i < swi_num; i++){
			rc = nvs_read(&fs, SWI_DATA_START_ID + i, &swi_keys[i], sizeof(swi_key));
			if (rc > 0) printk("Read key on SWI%d: ADC: 0x%04X, Code: 0x%04X\n",
					swi_keys[i].swi, swi_keys[i].val, swi_keys[i].key);
			else printk("Error %d reading key %d.\n", rc, i);
		}
	}
}

void ign_thread(void){
	int cnt;
	printk("ign_thread: started\n");
	while (true){
		printk("ign_thread: Waiting.\n");
		k_sem_take(&ign_sem, K_SECONDS(10));
		cnt = 15;
		do {
			printk("Feeding the dog.\n");
			wdt_feed(wdt, wdt_channel_id);
			if (gpio_pin_get(porta, PIN_IN_IGN)){
				gpio_pin_set(porta, PIN_OUT_14V, 1);
				gpio_pin_set(porta, PIN_OUT_5V, 1);
				gpio_pin_set(porta, PIN_OUT_UNMUTE, 1);
				gpio_pin_set(porta, PIN_OUT_AMP, 1);
				goto outercontinue;
			} else {
				gpio_pin_set(porta, PIN_OUT_UNMUTE, 0);
				gpio_pin_set(porta, PIN_OUT_AMP, 0);
			}
			printk("Ignition OFF, counting down: %d\n", cnt);
			k_sleep(K_SECONDS(1));
			cnt--;
		} while (cnt > 0);
		if (!gpio_pin_get(porta, PIN_IN_IGN)){
			printk("Shutdown.\n");
			gpio_pin_set(porta, PIN_OUT_14V, 0);
			gpio_pin_set(porta, PIN_OUT_5V, 0);
		}
outercontinue:
		continue;
	}
}
void ign_handler(struct device *dev, struct gpio_callback *cb, uint32_t pins){
	printk("Ignition INT\n");
	k_sem_give(&ign_sem);
}
static struct gpio_callback int_cb_data;

void main(void){
	printk("96carboard startup.\n");

	// First thing... setup the watchdog timer.
	wdt = device_get_binding("WATCHDOG_0");
	if (!wdt){
		printk("WATCHDOG_0: Failed to get device binding\n");
		sys_reboot(0);
	}
	printk("Device is %p, name is %s\n", wdt, wdt->name);

	struct wdt_timeout_cfg wdt_config;
	wdt_config.flags = WDT_FLAG_RESET_SOC;
	wdt_config.window.min = 0U;
	wdt_config.window.max = 15000U; // 15 seconds
	wdt_channel_id = wdt_install_timeout(wdt, &wdt_config);
	if (wdt_channel_id < 0 || wdt_setup(wdt, 0) < 0){
		printk("Watchdog setup error\n");
		sys_reboot(0);
	}
	wdt_feed(wdt, wdt_channel_id);

	init_load_nvs();

	pwm0 = device_get_binding("SAMD_PWM_0");
	if (!pwm0){
		printk("SAMD_PWM_0: Failed to get device binding\n");
		sys_reboot(0);
	}
	printk("Device is %p, name is %s\n", pwm0, pwm0->name);

	pwm1 = device_get_binding("SAMD_PWM_1");
	if (!pwm1){
		printk("SAMD_PWM_1: Failed to get device binding\n");
		sys_reboot(0);
	}
	printk("Device is %p, name is %s\n", pwm1, pwm1->name);

	adc = device_get_binding("ADC_0");
	if (!adc){
		printk("ADC_0: Failed to get device binding\n");
		sys_reboot(0);
	}
	printk("Device is %p, name is %s\n", adc, adc->name);

	i2c = device_get_binding("I2C_0");
	if (!i2c){
		printk("I2C_0: Failed to get device binding\n");
		sys_reboot(0);
	}
	printk("Device is %p, name is %s\n", i2c, i2c->name);

	hid_key = device_get_binding("HID_0");
	if (!hid_key){
		printk("HID_0: Failed to get device binding\n");
		sys_reboot(0);
	}
	printk("Device is %p, name is %s\n", hid_key, hid_key->name);

	hid_touch = device_get_binding("HID_1");
	if (!hid_touch){
		printk("HID_1: Failed to get device binding\n");
		sys_reboot(0);
	}
	printk("Device is %p, name is %s\n", hid_touch, hid_touch->name);

	hid_conf = device_get_binding("HID_2");
	if (!hid_conf){
		printk("HID_2: Failed to get device binding\n");
		sys_reboot(0);
	}
	printk("Device is %p, name is %s\n", hid_conf, hid_conf->name);

	hid_backl = device_get_binding("HID_3");
	if (!hid_backl){
		printk("HID_3: Failed to get device binding\n");
		sys_reboot(0);
	}
	printk("Device is %p, name is %s\n", hid_backl, hid_backl->name);

	hid_fan = device_get_binding("HID_4");
	if (!hid_fan){
		printk("HID_4: Failed to get device binding\n");
		sys_reboot(0);
	}
	printk("Device is %p, name is %s\n", hid_fan, hid_fan->name);

	if (tsconf.enabled){
		gt911 = device_get_binding("GT911");
		if (!gt911){
			printk("GT911: Failed to get device binding\n");
			sys_reboot(0);
		}
		printk("Device is %p, name is %s\n", gt911, gt911->name);
	}

	porta = device_get_binding("PORTA");
	if (!porta){
		printk("PORTA: Failed to get device binding\n");
		sys_reboot(0);
	}
	printk("Device is %p, name is %s\n", porta, porta->name);

	gpio_pin_configure(porta, PIN_IN_PA02, GPIO_INPUT);
	gpio_pin_configure(porta, PIN_IN_PA03, GPIO_INPUT);
	gpio_pin_configure(porta, PIN_IN_PA04, GPIO_INPUT);
	gpio_pin_configure(porta, PIN_IN_PA05, GPIO_INPUT);

	gpio_pin_configure(porta, PIN_OUT_14V, GPIO_OUTPUT);
	gpio_pin_configure(porta, PIN_OUT_5V, GPIO_OUTPUT);
	gpio_pin_configure(porta, PIN_OUT_3V3, GPIO_OUTPUT);
	gpio_pin_configure(porta, PIN_OUT_UNMUTE, GPIO_OUTPUT);
	gpio_pin_configure(porta, PIN_OUT_AMP, GPIO_OUTPUT);
	gpio_pin_configure(porta, PIN_IN_IGN, GPIO_INPUT | GPIO_PULL_DOWN);
	gpio_pin_configure(porta, PIN_IN_HEADLIGHT, GPIO_INPUT | GPIO_PULL_DOWN);
	gpio_pin_configure(porta, PIN_IN_REVERSE, GPIO_INPUT | GPIO_PULL_DOWN);

	gpio_pin_set(porta, PIN_OUT_14V, 1);
	gpio_pin_set(porta, PIN_OUT_5V, 1);
	gpio_pin_set(porta, PIN_OUT_3V3, 1);
	gpio_pin_set(porta, PIN_OUT_UNMUTE, 1);
	gpio_pin_set(porta, PIN_OUT_AMP, 1);

	gpio_pin_interrupt_configure(porta, PIN_IN_IGN, GPIO_INT_EDGE_BOTH);
	gpio_init_callback(&int_cb_data, ign_handler, BIT(PIN_IN_IGN));
	gpio_add_callback(porta, &int_cb_data);
	k_sem_give(&ign_sem);

	if (adc_channel_setup(adc, &m_channel_cfg) != 0){
		printk("Setting up ADC failed\n");
		sys_reboot(0);
	}

	samd_pwm_setdiv(pwm0, 5);
	samd_pwm_portcfg(pwm0, 0, PIN_PWM_FAN);
	samd_pwm_setpwm(pwm0, 0, PIN_PWM_FAN, PERIOD, PERIOD / 2); // fan at 50%

	samd_pwm_setdiv(pwm1, 5);
	samd_pwm_portcfg(pwm1, 0, PIN_PWM_BACKLIGHT);
	samd_pwm_setpwm(pwm1, 0,  PIN_PWM_BACKLIGHT, PERIOD, 0); // backlight at 100%

	IRQ_CONNECT(AC_IRQn, 2, ac_isr, NULL, 0);
	irq_enable(AC_IRQn);

	setup_ac();

	if (usb_enable(status_cb) != 0) {
		printk("Failed to enable USB");
		sys_reboot(0);
	}

	usb_hid_register_device(hid_key, hid_consumer_report_desc, sizeof(hid_consumer_report_desc), &ops);
	usb_hid_init(hid_key);
	usb_hid_register_device(hid_conf, hid_config_report_desc, sizeof(hid_config_report_desc), &ops_conf);
	usb_hid_init(hid_conf);
	usb_hid_register_device(hid_backl, hid_backl_report_desc, sizeof(hid_backl_report_desc), &ops_backl);
	usb_hid_init(hid_backl);
	usb_hid_register_device(hid_fan, hid_fan_report_desc, sizeof(hid_fan_report_desc), &ops_fan);
	usb_hid_init(hid_fan);

	if (tsconf.enabled){
		gt911_config(gt911, i2c, (tsconf.add0x14 ? 0x14 : 0x5d), porta, 18,
				(tsconf.reset ? porta : NULL), 19);
		gt911_hid_config(gt911, hid_touch, tsconf.inv_x, tsconf.inv_y, tsconf.swap_xy,
				tsconf.b_left, tsconf.b_right, tsconf.b_top, tsconf.b_bottom);
	} else {
		// If the touchscreen isn't being configured, set this device as a generic HID
		// in order to prevent errors on the host.
		usb_hid_register_device(hid_touch, hid_config_report_desc, sizeof(hid_config_report_desc), &ops);
		usb_hid_init(hid_touch);
	}

	dev_ready = 1;
	printk("Completed main()\nWaiting for USB.\n");
	k_sleep(K_SECONDS(60));
	if (!usb_ready){
		printk("No USB connected, resetting.\n");
		sys_reboot(0);
	}
}

// returns keycode
uint16_t key_lookup(uint8_t swi, uint16_t val){
	uint8_t i;
	for (i = 0; i < swi_num; i++){
		if (swi_keys[i].swi == swi &&
				val > swi_keys[i].val - SWI_TOLERANCE &&
				val < swi_keys[i].val + SWI_TOLERANCE)
			return swi_keys[i].key;
	}
	return 0x0000;
}

void key_thread(void){
	uint8_t report[5];
	uint16_t key;
	printk("key_thread: started\n");
	memset(report, 0, sizeof(report));
	report[0] = CONSUMER_KEY_REPORT_ID;
	while (true){
		k_sem_take(&key_sem, K_FOREVER);
		if (swidump || !usb_ready) continue;
		k_sleep(K_MSEC(10));

		printk("Sending consumer key report\n");

		if (!AC->STATUSA.bit.STATE0){
			// The button has been pressed.
			m_channel_cfg.input_positive = ADC_INPUTCTRL_MUXPOS_PIN4;
			adc_channel_setup(adc, &m_channel_cfg);
			if (adc_read(adc, &sequence) == 0)
				printk("ADC PA04: 0x%04x\n", m_sample_buffer[0]);

			key = key_lookup(0, m_sample_buffer[0]);
			report[2] = key >> 8;	// MSB
			report[1] = key & 0xff;	// LSB
			printk("PA04 is pressed, keycode: %04X\n", key);
			if (key == 0x0){
				printk("Key not found.\n");
				continue;
			}
		} else {
			// The button has been released.
			report[2] = 0x0;
			report[1] = 0x0;
			printk("PA04 is released.\n");
		}

		if (!AC->STATUSA.bit.STATE1){
			// The button has been pressed.
			m_channel_cfg.input_positive = ADC_INPUTCTRL_MUXPOS_PIN5;
			adc_channel_setup(adc, &m_channel_cfg);
			if (adc_read(adc, &sequence) == 0)
				printk("ADC PA05: 0x%04x\n", m_sample_buffer[0]);

			key = key_lookup(1, m_sample_buffer[0]);
			report[4] = key >> 8;	// MSB
			report[3] = key & 0xff;	// LSB
			printk("PA05 is pressed, keycode: %04X\n", key);
			if (key == 0x0){
				printk("Key not found.\n");
				continue;
			}
		} else {
			// The button has been released.
			report[4] = 0x0;
			report[3] = 0x0;
			printk("PA05 is released.\n");
		}

		hid_int_ep_write(hid_key, report, sizeof(report), NULL);
	}
}

/*
 * dev_thread executes 5 seconds after system startup. The flag
 * dev_ready is set true near the end of main(). If main() doesn't
 * get to that point within 5 seconds, then something is wrong.
 */
void dev_thread(void){
	if (!dev_ready){
	       printk("dev_thread: something went wrong during startup, rebooting.\n");
	       sys_reboot(0);
	}
	printk("dev_thread: system startup completed.\n");
}

K_THREAD_DEFINE(ign_id, 1024, ign_thread, NULL, NULL, NULL, 0, 0, 1000);
K_THREAD_DEFINE(key_id, 1024, key_thread, NULL, NULL, NULL, 0, 0, 1500);
K_THREAD_DEFINE(swi_id, 1024, swi_thread, NULL, NULL, NULL, 0, 0, 2000);
K_THREAD_DEFINE(dev_id, 1024, dev_thread, NULL, NULL, NULL, 0, 0, 5000);

