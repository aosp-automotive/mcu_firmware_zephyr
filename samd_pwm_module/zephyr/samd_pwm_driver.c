/*
 * Copyright (c) 2020 Adam Serbinski
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include "samd_pwm_driver.h"
#include <zephyr/types.h>
#include <soc.h>

struct samd_pwm_config {
	Tcc *tcc;
	const uint8_t gclk_id;
	const uint32_t apbcmask;
};

static const struct samd_pwm_config samd_pwm_tcc0 = {
	.tcc = TCC0,
	.gclk_id = TCC0_GCLK_ID,
	.apbcmask = PM_APBCMASK_TCC0
};

static const struct samd_pwm_config samd_pwm_tcc1 = {
	.tcc = TCC1,
	.gclk_id = TCC1_GCLK_ID,
	.apbcmask = PM_APBCMASK_TCC1
};

static const struct samd_pwm_config samd_pwm_tcc2 = {
	.tcc = TCC2,
	.gclk_id = TCC2_GCLK_ID,
	.apbcmask = PM_APBCMASK_TCC2
};

/*
 *  CC[0]:	WO[0], WO[4]
 *  CC[1]:	WO[1], WO[5]
 *  CC[2]:	WO[2], WO[6]
 *  CC[3]:	WO[3], WO[7]
 *
 *
 *  These are all of the pins available for PWM on the SAMD21E:
 *
 *  Peripheral FN	E			F
 *
 *  PA00:	TCC2/WO[0]: CC[0]
 *  PA01:	TCC2/WO[1]: CC[1]
 *  PA02:
 *  PA03:
 *  PA04:	TCC0/WO[0]: CC[0]
 *  PA05:	TCC0/WO[1]: CC[1]
 *  PA06:	TCC1/WO[0]: CC[0]
 *  PA07:	TCC1/WO[1]: CC[1]
 *  PA08:	TCC0/WO[0]: CC[0]	TCC1/WO[2]: CC[2]
 *  PA09:	TCC0/WO[1]: CC[1]	TCC1/WO[3]: CC[3]
 *  PA10:	TCC1/WO[0]: CC[0]	TCC0/WO[2]: CC[2]
 *  PA11:	TCC1/WO[1]: CC[1]	TCC0/WO[3]: CC[3]
 *  PA12:	TCC2/WO[0]: CC[0]	TCC0/WO[6]: CC[2]
 *  PA13:	TCC2/WO[1]: CC[1]	TCC0/WO[7]: CC[3]
 *  PA14:				TCC0/WO[4]: CC[0]
 *  PA15:				TCC0/WO[5]: CC[1]
 *  PA16:	TCC2/WO[0]: CC[0]	TCC0/WO[6]: CC[2]
 *  PA17:	TCC2/WO[1]: CC[1]	TCC0/WO[7]: CC[3]
 *  PA18:				TCC0/WO[2]: CC[2]
 *  PA19:				TCC0/WO[3]: CC[3]
 *  PA20:				TCC0/WO[6]: CC[2]
 *  PA21:				TCC0/WO[7]: CC[3]
 *  PA22:				TCC0/WO[4]: CC[0]
 *  PA23:				TCC0/WO[5]: CC[1]
 *  PA24:				TCC1/WO[2]: CC[2]
 *  PA25:				TCC1/WO[3]: CC[3]
 *  PA26:
 *  PA27:
 *  PA28:
 *  PA29:
 *  PA30:	TCC1/WO[0]: CC[0]
 *  PA31:	TCC1/WO[3]: CC[3]
 *
 *  PB00:
 *  PB01:
 *  PB02:
 *  PB03:
 *  PB04:
 *  PB05:
 *  PB06:
 *  PB07:
 *  PB08:
 *  PB09:
 *  PB10:				TCC0/WO[4]: CC[0]
 *  PB11:				TCC0/WO[5]: CC[1]
 *  PB12:				TCC0/WO[6]: CC[2]
 *  PB13:				TCC0/WO[7]: CC[3]
 *  PB14:
 *  PB15:
 *  PB16:				TCC0/WO[4]: CC[0]
 *  PB17:				TCC0/WO[5]: CC[1]
 *  PB18:
 *  PB19:
 *  PB20:
 *  PB21:
 *  PB22:
 *  PB23:
 *  PB24:
 *  PB25:
 *  PB26:
 *  PB27:
 *  PB28:
 *  PB29:
 *  PB30:	TCC0/WO[0]: CC[0]	TCC1/WO[2]: CC[2]
 *  PB31:	TCC0/WO[1]: CC[1]	TCC1/WO[3]: CC[3]
 *
 *  WO[x] is used to select the proper CC[y].
 *
 *  Peripheral FN "F" for PORTA pins 10-23 on TCC0, 8,9,24,25 on TCC1, all else on FN "E"
 *  Peripheral FN "F" for PORTB except pins 30/31 on TCC0.
 */

// CC[] idx corresponding to pin number
static int CCe[2][32] = {
	{ 0, 1,-1,-1, 0,
	  1, 0, 1, 0, 1,
	  0, 1, 0, 1,-1,
	 -1, 0, 1,-1,-1,
	 -1,-1,-1,-1,-1,
	 -1,-1,-1,-1,-1,
	  0, 3},
	{-1,-1,-1,-1,-1,
	 -1,-1,-1,-1,-1,
	 -1,-1,-1,-1,-1,
	 -1,-1,-1,-1,-1,
	 -1,-1,-1,-1,-1,
	 -1,-1,-1,-1,-1,
	  0, 1}
};
static int CCf[2][32] = {
	{-1,-1,-1,-1,-1,
	-1,-1,-1, 2, 3,
	 2, 3, 2, 3, 0,
	 1, 2, 3, 2, 3,
	 2, 3, 0, 1, 2,
	 3,-1,-1,-1,-1,
	-1,-1},
	{-1,-1,-1,-1,-1,
	 -1,-1,-1,-1,-1,
	  0, 1, 2, 3,-1,
	 -1, 0, 1,-1,-1,
	 -1,-1,-1,-1,-1,
	 -1,-1,-1,-1,-1,
	  2, 3}
};

static int init(struct device *dev){
	const struct samd_pwm_config *cfg = dev->config_info;
	Tcc *const tcc = cfg->tcc;

	GCLK->GENCTRL.reg = GCLK_GENCTRL_IDC |          // Improve duty cycle
			    GCLK_GENCTRL_GENEN |        // Enable generic clock gen
			    GCLK_GENCTRL_SRC_DFLL48M |  // Select 48MHz as source
			    GCLK_GENCTRL_ID(4);         // Select GCLK4
	while (GCLK->STATUS.bit.SYNCBUSY);              // Wait for synchronization
 
	// Set clock divider of 1 to generic clock generator 4
	GCLK->GENDIV.reg = GCLK_GENDIV_DIV(1) |         // Divide 48 MHz by 1
			   GCLK_GENDIV_ID(4);           // Apply to GCLK4 4
	while (GCLK->STATUS.bit.SYNCBUSY);              // Wait for synchronization

	/* power on the device */
	PM->APBCMASK.reg |= cfg->apbcmask;
	GCLK->CLKCTRL.reg = (uint16_t)(GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN_GCLK4 | GCLK_CLKCTRL_ID(cfg->gclk_id));
	while (GCLK->STATUS.bit.SYNCBUSY) {}

	/* reset TCC module */
	tcc->CTRLA.reg = TCC_CTRLA_SWRST;
	while (tcc->SYNCBUSY.reg & TCC_SYNCBUSY_SWRST) {}
	tcc->CTRLBCLR.reg = TCC_CTRLBCLR_DIR;     /* count up */
	while (tcc->SYNCBUSY.reg & TCC_SYNCBUSY_CTRLB) {}

	return 0;
}

static void samd_pwm_setdiv_impl(struct device *dev, uint8_t divider){
	const struct samd_pwm_config *cfg = dev->config_info;
	Tcc *const tcc = cfg->tcc;

	/* configure the TCC device */
	tcc->CTRLA.reg = (TCC_CTRLA_PRESCSYNC_GCLK_Val | ((uint32_t)divider) << TCC_CTRLA_PRESCALER_Pos);
	/* select the waveform generation mode -> normal PWM */
	tcc->WAVE.reg = (TCC_WAVE_WAVEGEN_NPWM);
	while (tcc->SYNCBUSY.reg & TCC_SYNCBUSY_WAVE) {}
}

static void samd_pwm_portcfg_impl(struct device *dev, uint8_t group, uint8_t pin){
	const struct samd_pwm_config *cfg = dev->config_info;
	Tcc *const tcc = cfg->tcc;
	uint8_t portF = 0;
	if ((group == 0 && 
			((tcc == TCC0 && 10 <= pin && 23 >= pin) ||
			(tcc == TCC1 && (pin == 8 || pin == 9 || pin == 24 || pin == 25)))
	    ) ||
	    (group == 1 &&
			!(pin == 30 || pin == 31)
	    )
	) portF = 1;

	// Group[0] --> PA. Group[1] --> PB.
	PORT->Group[group].DIRSET.reg = 1UL << pin;
	PORT->Group[group].OUTCLR.reg = 1UL << pin;
	PORT->Group[group].PINCFG[pin].reg |= PORT_PINCFG_PMUXEN;
	if (pin & 1) // odd channel/pin number
		PORT->Group[group].PMUX[pin / 2U].reg |= (portF ? PORT_PMUX_PMUXO_F : PORT_PMUX_PMUXO_E);
	else
		PORT->Group[group].PMUX[pin / 2U].reg |= (portF ? PORT_PMUX_PMUXE_F : PORT_PMUX_PMUXE_E);
}

static void samd_pwm_setpwm_impl(struct device *dev, uint8_t group, uint8_t pin, uint32_t period, uint32_t pulse){
	const struct samd_pwm_config *cfg = dev->config_info;
	Tcc *const tcc = cfg->tcc;
	uint8_t portF = 0;
	if ((group == 0 && 
			((tcc == TCC0 && 10 <= pin && 23 >= pin) ||
			(tcc == TCC1 && (pin == 8 || pin == 9 || pin == 24 || pin == 25)))
	    ) ||
	    (group == 1 &&
			!(pin == 30 || pin == 31)
	    )
	) portF = 1;

	/* set the selected period */
	tcc->PER.reg = period;
	while (tcc->SYNCBUSY.reg & TCC_SYNCBUSY_PER) {}
	tcc->CC[(portF ? CCf[group][pin] : CCe[group][pin])].reg = pulse;
	/* start PWM operation */
	tcc->CTRLA.reg |= (TCC_CTRLA_ENABLE);
}

DEVICE_AND_API_INIT(SAMD_PWM_0, "SAMD_PWM_0", init, NULL, &samd_pwm_tcc0,
		    PRE_KERNEL_1, CONFIG_KERNEL_INIT_PRIORITY_DEVICE,
		    &((struct samd_pwm_driver_api){
			   .setdiv = samd_pwm_setdiv_impl,
			   .portcfg = samd_pwm_portcfg_impl,
			   .setpwm = samd_pwm_setpwm_impl
		    }));

DEVICE_AND_API_INIT(SAMD_PWM_1, "SAMD_PWM_1", init, NULL, &samd_pwm_tcc1,
		    PRE_KERNEL_1, CONFIG_KERNEL_INIT_PRIORITY_DEVICE,
		    &((struct samd_pwm_driver_api){
			   .setdiv = samd_pwm_setdiv_impl,
			   .portcfg = samd_pwm_portcfg_impl,
			   .setpwm = samd_pwm_setpwm_impl
		    }));

DEVICE_AND_API_INIT(SAMD_PWM_2, "SAMD_PWM_2", init, NULL, &samd_pwm_tcc2,
		    PRE_KERNEL_1, CONFIG_KERNEL_INIT_PRIORITY_DEVICE,
		    &((struct samd_pwm_driver_api){
			   .setdiv = samd_pwm_setdiv_impl,
			   .portcfg = samd_pwm_portcfg_impl,
			   .setpwm = samd_pwm_setpwm_impl
		    }));
