/*
 * Copyright (c) 2020 Adam Serbinski
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#ifndef __SAMD_PWM_DRIVER_H__
#define __SAMD_PWM_DRIVER_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <device.h>

struct samd_pwm_driver_api {
	void (*setdiv)(struct device *dev, uint8_t divider);
	void (*portcfg)(struct device *dev, uint8_t group, uint8_t pin);
	void (*setpwm)(struct device *dev, uint8_t group, uint8_t pin, uint32_t period, uint32_t pulse);
};

__syscall void samd_pwm_setdiv(struct device *dev, uint8_t divider);
static inline void z_impl_samd_pwm_setdiv(struct device *dev, uint8_t divider){
	const struct samd_pwm_driver_api *api = dev->driver_api;
	__ASSERT(api->setdiv, "Callback pointer 'setdiv' should not be NULL");
	api->setdiv(dev, divider);
}

__syscall void samd_pwm_portcfg(struct device *dev, uint8_t group, uint8_t pin);
static inline void z_impl_samd_pwm_portcfg(struct device *dev, uint8_t group, uint8_t pin){
	const struct samd_pwm_driver_api *api = dev->driver_api;
	__ASSERT(api->portcfg, "Callback pointer 'portcfg' should not be NULL");
	api->portcfg(dev, group, pin);
}

__syscall void samd_pwm_setpwm(struct device *dev, uint8_t group, uint8_t pin, uint32_t period, uint32_t pulse);
static inline void z_impl_samd_pwm_setpwm(struct device *dev, uint8_t group, uint8_t pin, uint32_t period, uint32_t pulse){
	const struct samd_pwm_driver_api *api = dev->driver_api;
	__ASSERT(api->setpwm, "Callback pointer 'setpwm' should not be NULL");
	api->setpwm(dev, group, pin, period, pulse);
}

#ifdef __cplusplus
}
#endif

#include <syscalls/samd_pwm_driver.h>

#endif /* __SAMD_PWM_DRIVER_H__ */
