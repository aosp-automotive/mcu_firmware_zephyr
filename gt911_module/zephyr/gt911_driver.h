/*
 * Copyright (c) 2020 Adam Serbinski
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#ifndef __GT911_DRIVER_H__
#define __GT911_DRIVER_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <device.h>
#include <drivers/gpio.h>

#define GOODIX_CONTACT_SIZE				8
#define GOODIX_MAX_CONTACTS				5

struct gt911_driver_api {
	void (*config)(struct device *dev, struct device *i2c, uint16_t address,
		struct device *port_int, gpio_pin_t pin_int,
		struct device *port_rst, gpio_pin_t pin_rst);
	void (*hid_config)(struct device *dev, struct device *hid, uint8_t inv_x,
		uint8_t inv_y, uint8_t swap_xy, uint16_t b_left, uint16_t b_right,
		uint16_t b_top, uint16_t b_bottom);
	void (*hid_enable)(struct device *dev);
	void (*poll)(struct device *dev);
};

__syscall void gt911_config(struct device *dev, struct device *i2c, uint16_t address,
		struct device *port_int, gpio_pin_t pin_int,
		struct device *port_rst, gpio_pin_t pin_rst);
static inline void z_impl_gt911_config(struct device *dev, struct device *i2c, uint16_t address,
		struct device *port_int, gpio_pin_t pin_int,
		struct device *port_rst, gpio_pin_t pin_rst){
	const struct gt911_driver_api *api = dev->driver_api;
	__ASSERT(api->config, "Callback pointer 'config' should not be NULL");
	api->config(dev, i2c, address, port_int, pin_int, port_rst, pin_rst);
}

__syscall void gt911_hid_config(struct device *dev, struct device *hid, uint8_t inv_x,
		uint8_t inv_y, uint8_t swap_xy, uint16_t b_left, uint16_t b_right,
		uint16_t b_top, uint16_t b_bottom);
static inline void z_impl_gt911_hid_config(struct device *dev, struct device *hid, uint8_t inv_x,
		uint8_t inv_y, uint8_t swap_xy, uint16_t b_left, uint16_t b_right,
		uint16_t b_top, uint16_t b_bottom){
	const struct gt911_driver_api *api = dev->driver_api;
	__ASSERT(api->hid_config, "Callback pointer 'hid_config' should not be NULL");
	api->hid_config(dev, hid, inv_x, inv_y, swap_xy, b_left, b_right, b_top, b_bottom);
}

__syscall void gt911_hid_enable(struct device *dev);
static inline void z_impl_gt911_hid_enable(struct device *dev){
	const struct gt911_driver_api *api = dev->driver_api;
	__ASSERT(api->hid_enable, "Callback pointer 'hid_enable' should not be NULL");
	api->hid_enable(dev);
}

__syscall void gt911_poll(struct device *dev);
static inline void z_impl_gt911_poll(struct device *dev){
	const struct gt911_driver_api *api = dev->driver_api;
	__ASSERT(api->poll, "Callback pointer 'poll' should not be NULL");
	api->poll(dev);
}

#ifdef __cplusplus
}
#endif

#include <syscalls/gt911_driver.h>

#endif /* __GT911_DRIVER_H__ */
