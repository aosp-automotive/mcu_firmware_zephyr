/*  Top level collection for 5 point Multi-Touch Digitizer device.
 *  -------------------------------------------------------------------
 *  Format for multi-touch digitizer input reports using this report descriptor:
 *
 *  Byte[0] = Report ID == MULTI_TOUCH_DATA_REPORT_ID = 0x01
 *
 *  5 contact points in groups of 6 bytes:
 *  Byte[1] = Bits7-3: pad bits (unused), Bit1:In Range, Bit0:Tip Switch
 *  Byte[2] = Contact identifier number (see note above)
 *  Byte[3] = X-coordinate LSB
 *  Byte[4] = X-coordinate MSB
 *  Byte[5] = Y-coordinate LSB
 *  Byte[6] = Y-coordinate MSB
 *  -- repeated 5 times for 5 points
 *
 *  Byte[31]= 8-bit number indicating how many of the above contact points are valid.
 *  Valid points come first. I suspect that this should be equal to the number of points
 *  reportable by the device. I.e., always 0x05 for a 5-point touchscreen like a GT911.
 *  The state of the in-range and tip-switch bits indicate whether the point is touched
 *  or not.
 */

#define MULTI_TOUCH_DATA_REPORT_ID		0x01
#define HID_MULTITOUCH_REPORT_DESC_LEN		336
#define HID_MULTITOUCH_REPORT_DESC(xhi,xlo,yhi,ylo){	\
	0x05, 0x0D,					\
	0x09, 0x04,					\
	0xA1, 0x01,					\
	0x85, 0x01,					\
	0x09, 0x22,					\
	0xA1, 0x02,					\
	0x09, 0x42,					\
	0x15, 0x00,					\
	0x25, 0x01,					\
	0x75, 0x01,					\
	0x95, 0x01,					\
	0x81, 0x02,					\
	0x09, 0x32,					\
	0x81, 0x02,					\
	0x95, 0x06,					\
	0x81, 0x03,					\
	0x75, 0x08,					\
	0x09, 0x51,					\
	0x95, 0x01,					\
	0x81, 0x02,					\
	0x05, 0x01,					\
	0x26, xlo, xhi,					\
	0x75, 0x10,					\
	0x55, 0x0E,					\
	0x65, 0x33,					\
	0x09, 0x30,					\
	0x35, 0x00,					\
	0x46, xlo, xhi,					\
	0x81, 0x02,					\
	0x26, ylo, yhi,					\
	0x46, ylo, yhi,					\
	0x09, 0x31,					\
	0x81, 0x02,					\
	0xC0,						\
	0xA1, 0x02,					\
	0x05, 0x0D,					\
	0x09, 0x42,					\
	0x15, 0x00,					\
	0x25, 0x01,					\
	0x75, 0x01,					\
	0x95, 0x01,					\
	0x81, 0x02,					\
	0x09, 0x32,					\
	0x81, 0x02,					\
	0x95, 0x06,					\
	0x81, 0x03,					\
	0x75, 0x08,					\
	0x09, 0x51,					\
	0x95, 0x01,					\
	0x81, 0x02,					\
	0x05, 0x01,					\
	0x26, xlo, xhi,					\
	0x75, 0x10,					\
	0x55, 0x0E,					\
	0x65, 0x33,					\
	0x09, 0x30,					\
	0x35, 0x00,					\
	0x46, xlo, xhi,					\
	0x81, 0x02,					\
	0x26, ylo, yhi,					\
	0x46, ylo, yhi,					\
	0x09, 0x31,					\
	0x81, 0x02,					\
	0xC0,						\
	0xA1, 0x02,					\
	0x05, 0x0D,					\
	0x09, 0x42,					\
	0x15, 0x00,					\
	0x25, 0x01,					\
	0x75, 0x01,					\
	0x95, 0x01,					\
	0x81, 0x02,					\
	0x09, 0x32,					\
	0x81, 0x02,					\
	0x95, 0x06,					\
	0x81, 0x03,					\
	0x75, 0x08,					\
	0x09, 0x51,					\
	0x95, 0x01,					\
	0x81, 0x02,					\
	0x05, 0x01,					\
	0x26, xlo, xhi,					\
	0x75, 0x10,					\
	0x55, 0x0E,					\
	0x65, 0x33,					\
	0x09, 0x30,					\
	0x35, 0x00,					\
	0x46, xlo, xhi,					\
	0x81, 0x02,					\
	0x26, ylo, yhi,					\
	0x46, ylo, yhi,					\
	0x09, 0x31,					\
	0x81, 0x02,					\
	0xC0,						\
	0xA1, 0x02,					\
	0x05, 0x0D,					\
	0x09, 0x42,					\
	0x15, 0x00,					\
	0x25, 0x01,					\
	0x75, 0x01,					\
	0x95, 0x01,					\
	0x81, 0x02,					\
	0x09, 0x32,					\
	0x81, 0x02,					\
	0x95, 0x06,					\
	0x81, 0x03,					\
	0x75, 0x08,					\
	0x09, 0x51,					\
	0x95, 0x01,					\
	0x81, 0x02,					\
	0x05, 0x01,					\
	0x26, xlo, xhi,					\
	0x75, 0x10,					\
	0x55, 0x0E,					\
	0x65, 0x33,					\
	0x09, 0x30,					\
	0x35, 0x00,					\
	0x46, xlo, xhi,					\
	0x81, 0x02,					\
	0x26, ylo, yhi,					\
	0x46, ylo, yhi,					\
	0x09, 0x31,					\
	0x81, 0x02,					\
	0xC0,						\
	0xA1, 0x02,					\
	0x05, 0x0D,					\
	0x09, 0x42,					\
	0x15, 0x00,					\
	0x25, 0x01,					\
	0x75, 0x01,					\
	0x95, 0x01,					\
	0x81, 0x02,					\
	0x09, 0x32,					\
	0x81, 0x02,					\
	0x95, 0x06,					\
	0x81, 0x03,					\
	0x75, 0x08,					\
	0x09, 0x51,					\
	0x95, 0x01,					\
	0x81, 0x02,					\
	0x05, 0x01,					\
	0x26, xlo, xhi,					\
	0x75, 0x10,					\
	0x55, 0x0E,					\
	0x65, 0x33,					\
	0x09, 0x30,					\
	0x35, 0x00,					\
	0x46, xlo, xhi,					\
	0x81, 0x02,					\
	0x26, ylo, yhi,					\
	0x46, ylo, yhi,					\
	0x09, 0x31,					\
	0x81, 0x02,					\
	0xC0,						\
	0x05, 0x0D,					\
	0x09, 0x54,					\
	0x95, 0x01,					\
	0x75, 0x08,					\
	0x25, 0x05,					\
	0x81, 0x02,					\
	0xC0,						\
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, \
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, \
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, \
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, \
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, \
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, \
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, \
}

