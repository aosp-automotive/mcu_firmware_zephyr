/*
 * Copyright (c) 2020 Adam Serbinski
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include "gt911_driver.h"
#include "hid_touch.h"
#include <drivers/i2c.h>
#include <usb/usb_device.h>
#include <usb/class/usb_hid.h>
#include <zephyr/types.h>

static K_SEM_DEFINE(gt911_sem, 0, 1);
static K_SEM_DEFINE(multitouch_sem, 0, 1);
static K_SEM_DEFINE(hid_sem, 0, 1);

static uint8_t hid_mt[32];
static uint8_t usb_enabled = 0;

static struct gt911_data {
	struct device *i2c;
	uint16_t i2c_addr;
	uint16_t xres;
	uint16_t yres;
	uint8_t inv_x;
	uint8_t inv_y;
	uint8_t swap_xy;
	uint16_t b_left;
	uint16_t b_right;
	uint16_t b_top;
	uint16_t b_bottom;

	struct device *hid;

	struct device *port_int;
	gpio_pin_t pin_int;
	struct device *port_rst;
	gpio_pin_t pin_rst;
} data;

void mt_thread(void){
	//uint8_t i;
	while (true){
		k_sem_take(&multitouch_sem, K_FOREVER);
		//printk("Sending multitouch report:\n");
		//for (i=0; i<32; i++) printk("%02X ", hid_mt[i]);
		//printk("\n");
		if (usb_enabled) hid_int_ep_write(data.hid, hid_mt, sizeof(hid_mt), NULL);
		k_sem_give(&hid_sem);
	}
}

void int_handler(struct device *dev, struct gpio_callback *cb, uint32_t pins){
	k_sem_give(&gt911_sem);
}
static struct gpio_callback int_cb_data;

static int init(struct device *dev){
	return 0;
}

static void gt911_config_impl(struct device *dev, struct device *i2c, uint16_t address,
		struct device *port_int, gpio_pin_t pin_int,
		struct device *port_rst, gpio_pin_t pin_rst){
	data.i2c = i2c;
	data.i2c_addr = address;
	data.port_int = port_int;
	data.pin_int = pin_int;
	data.port_rst = port_rst;
	data.pin_rst = pin_rst;
	int ret, error = 0;

	if (address != 0x14 && address != 0x5d) return;

	if (port_int != NULL && port_rst != NULL){
		// Reset the touchscreen into the address 'address'.
		//
		// Device is in reset state when reset is pulled low for > 100 us.
		// Address is set to 0x14 if INT is driven HIGH for 100 us before
		// reset is released, and 5 ms after. Note ms vs us. If int is
		// pulled low throughout, address is 0x5d.

		ret = gpio_pin_configure(data.port_rst, data.pin_rst, GPIO_OUTPUT);
		if (ret != 0) return;
		ret = gpio_pin_configure(data.port_int, data.pin_int, GPIO_OUTPUT);
		if (ret != 0) return;

		gpio_pin_set(data.port_int, data.pin_int, 0);
		gpio_pin_set(data.port_rst, data.pin_rst, 0);
		k_sleep(K_MSEC(11));
		gpio_pin_set(data.port_int, data.pin_int, address == 0x14 ? 1 : 0);
		k_sleep(K_MSEC(1));
		gpio_pin_set(data.port_rst, data.pin_rst, 1);
		k_sleep(K_MSEC(6));
		gpio_pin_set(data.port_int, data.pin_int, 0);
		k_sleep(K_MSEC(51));

		ret = gpio_pin_configure(data.port_int, data.pin_int, GPIO_INPUT);
		if (ret != 0) return;
	}

	printk("Configured touchscreen %s at 0x%02X on %s\n", dev->name, data.i2c_addr, data.i2c->name);
	uint8_t writebuf[2] = {0x81, 0x40};
	uint8_t readbuf[12];
	error = i2c_write_read(data.i2c, data.i2c_addr, writebuf, 2, readbuf, 12);
	if (error){
		printk("I2C error reading config: %d\n", error);
		return;
	}
	data.xres = (readbuf[7] << 8) + readbuf[6];
	data.yres = (readbuf[9] << 8) + readbuf[8];
	printk("Device resolution: %dx%d\n", data.xres, data.yres);

	if (port_int != NULL){
		ret = gpio_pin_configure(data.port_int, data.pin_int, GPIO_INPUT);
		if (ret != 0) return;

		ret = gpio_pin_interrupt_configure(data.port_int, data.pin_int, GPIO_INT_EDGE_TO_ACTIVE);
		if (ret != 0) return;

		gpio_init_callback(&int_cb_data, int_handler, BIT(data.pin_int));
		gpio_add_callback(data.port_int, &int_cb_data);
	}
}

// gt911_hid_config MUST be run AFTER gt911_config.
static void gt911_hid_config_impl(struct device *dev, struct device *hid, uint8_t inv_x,
		uint8_t inv_y, uint8_t swap_xy, uint16_t b_left, uint16_t b_right,
		uint16_t b_top, uint16_t b_bottom){
	if (hid == NULL) return;
	data.hid = hid;
	data.inv_x = inv_x;
	data.inv_y = inv_y;
	data.swap_xy = swap_xy;
	data.b_left = b_left;
	data.b_right = b_right;
	data.b_top = b_top;
	data.b_bottom = b_bottom;

	if (swap_xy){
		uint16_t tmp = data.xres;
		data.xres = data.yres;
		data.yres = tmp;
	}

	// Origin (0,0) is in the upper-left corner of the screen.
	// b_left and b_right shifts mean reducing the reported width of the screen.
	// b_left requires the coordinates reported to = measured - b_left.
	// b_top and b_bottom shifts mean reducing the reported height of the screen.
	// b_top requires the coordinates reported to = measured - b_top.
	// Note: When the axis is inverted, the behavior of shifts are reversed.

	data.xres -= (data.b_left + data.b_right);
	data.yres -= (data.b_top + data.b_bottom);

	printk("Reporting max dimensions: %d X %d\n", data.xres, data.yres);

	uint8_t hid_touchscreen_report_desc[] = HID_MULTITOUCH_REPORT_DESC(data.xres >> 8,
			data.xres & 0xff, data.yres >> 8, data.yres & 0xff);
	usb_hid_register_device(hid, hid_touchscreen_report_desc,
			HID_MULTITOUCH_REPORT_DESC_LEN, NULL);
	usb_hid_init(hid);
}

static void gt911_hid_enable_impl(struct device *dev){
	usb_enabled = 1;
}

static void gt911_poll_impl(struct device *dev){
	k_sem_give(&gt911_sem);
}

void gt911_thread(void){
	int8_t i, j, contacts = 0, lastcontacts = 0;
	uint16_t tmp16;
	uint8_t tmp8[2];
	int error = 0;
	uint8_t regState = 0;
	uint8_t writebuf[3] = {0x81, 0x4e, 0x00};
	uint8_t points[GOODIX_MAX_CONTACTS*GOODIX_CONTACT_SIZE];

	while (true){
		k_sem_take(&gt911_sem, K_FOREVER);
		writebuf[1] = 0x4e;
		error = i2c_write_read(data.i2c, data.i2c_addr, writebuf, 2, &regState, 1);
		if (error) {
			printk("I2C error: %d\n", error);
			continue;
		}

		if (!(regState & 0x80)){
			continue;
		}

		contacts = regState & 0x0f;
		printk("contacts: %d\n", contacts);
		writebuf[1] = 0x4f;
		error = i2c_write_read(data.i2c, data.i2c_addr, writebuf, 2, points, GOODIX_CONTACT_SIZE * 5);
		if (error) continue;
		hid_mt[0] = MULTI_TOUCH_DATA_REPORT_ID;

		// The touchscreen doesn't send an "up" update. It just vanishes the contact that
		// is lifted. The host requires an "up" update, so we need to go through the
		// previous HID report, and find any contacts that are no longer pressed in THIS
		// report. If there are any, update the previous report to indicate lifted points
		// and resend it.
		uint8_t resend = 0;
		for (i = 0; i < 5; i++){
			if (hid_mt[(6*i)+1] != 0x0){ // if this was pressed on last pass...
				uint8_t mpress = 0;
				for (j = 0; j < contacts; j++){
					if (hid_mt[(6*i)+2] == points[(8*j)+0]){
						mpress = 1;
					}
				}
				if (mpress == 0 || contacts == 0){ // and does not remain pressed on this pass...
					hid_mt[(6*i)+1] = 0x0;
					resend = 1;
				}
			} else break;
		}
		if (resend){
			// hid_sem is used to make sure that we don't overwrite
			// hid_mt[] before it is sent to the host.
			k_sem_reset(&hid_sem);
			k_sem_give(&multitouch_sem);
			k_sem_take(&hid_sem, K_FOREVER);
			if (contacts == 0) continue;
		}

		for (i = 0; i < 5; i++){
//			printk(" - C(%d) - id: %d, X: 0x%02X%02X, Y: 0x%02X%02X, Size: 0x%02X%02X\n", i,
//					points[(8*i)+0],
//					points[(8*i)+2], points[(8*i)+1],
//					points[(8*i)+4], points[(8*i)+3],
//					points[(8*i)+6], points[(8*i)+5]);

			hid_mt[(6*i)+1] = i < contacts ? 0x03 : 0x00;		// <-- pressed or not pressed.
			if (i < contacts) hid_mt[(6*i)+2] = points[(8*i)+0];	// <-- contact id.

			hid_mt[(6*i)+3] = points[(8*i)+1];
			hid_mt[(6*i)+4] = points[(8*i)+2];
			hid_mt[(6*i)+5] = points[(8*i)+3];
			hid_mt[(6*i)+6] = points[(8*i)+4];

			// Here we can do some math on the xy values in hid_mt...
			//
			// First apply rotation,
			// Next apply inversions,
			// Last, apply borders.

			if (data.swap_xy){
				tmp8[0] = hid_mt[(6*i)+3];
				tmp8[1] = hid_mt[(6*i)+4];
				hid_mt[(6*i)+3] = hid_mt[(6*i)+5];
				hid_mt[(6*i)+4] = hid_mt[(6*i)+6];
				hid_mt[(6*i)+5] = tmp8[0];
				hid_mt[(6*i)+6] = tmp8[1];
			}

			if (data.inv_x){
				tmp16 = data.xres - ((hid_mt[(6*i)+4] << 8) + hid_mt[(6*i)+3]);
				hid_mt[(6*i)+4] = tmp16 >> 8;
				hid_mt[(6*i)+3] = tmp16 & 0xff;
			}

			if (data.inv_y){
				tmp16 = data.yres - ((hid_mt[(6*i)+6] << 8) + hid_mt[(6*i)+5]);
				hid_mt[(6*i)+6] = tmp16 >> 8;
				hid_mt[(6*i)+5] = tmp16 & 0xff;
			}

			if (data.b_left){
				tmp16 = (hid_mt[(6*i)+4] << 8) + hid_mt[(6*i)+3];
				tmp16 -= data.b_left;
				hid_mt[(6*i)+4] = tmp16 >> 8;
				hid_mt[(6*i)+3] = tmp16 & 0xff;
				printk("Reporting X: %d\n", tmp16);
			}

			if (data.b_top){
				tmp16 = (hid_mt[(6*i)+6] << 8) + hid_mt[(6*i)+5];
				tmp16 -= data.b_top;
				hid_mt[(6*i)+6] = tmp16 >> 8;
				hid_mt[(6*i)+5] = tmp16 & 0xff;
			}
		}

		hid_mt[31] = contacts;
		k_sem_give(&multitouch_sem);

		lastcontacts = contacts;

		writebuf[1] = 0x4e;
		i2c_write(data.i2c, writebuf, 3, data.i2c_addr);
	}
}

DEVICE_AND_API_INIT(GT911, "GT911", init, &data, NULL,
		    PRE_KERNEL_1, CONFIG_KERNEL_INIT_PRIORITY_DEVICE,
		    &((struct gt911_driver_api){
			   .config = gt911_config_impl,
			   .hid_config = gt911_hid_config_impl,
			   .hid_enable = gt911_hid_enable_impl,
			   .poll = gt911_poll_impl
		    }));

K_THREAD_DEFINE(gt911_id, 1024, gt911_thread, NULL, NULL, NULL, 0, 0, 0);
K_THREAD_DEFINE(mt_id, 1024, mt_thread, NULL, NULL, NULL, 0, 0, 0);
